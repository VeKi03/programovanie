import sdl2.ext
import numpy as np

class Bod:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # zobraz bod ako numpy
    def as_np(self):
        return np.array([self.x, self.y, 1])


class Tvar:
    def __init__(self, body, farba):
        self.body = body
        self.farba = farba

    def usecka(self, x1, y1, x2, y2):
        dx = abs(x2 - x1)
        dy = abs(y2 - y1)
        dlzka = dx if dx >= dy else dy
        if dlzka == 0:
            return
        dx = float(x2 - x1) / dlzka
        dy = float(y2 - y1) / dlzka
        x = x1
        y = y1
        i = 1
        while i <= dlzka:
            x = x + dx
            y = y + dy
            i = i + 1
            pixle[int(y)][int(x)] = self.farba

    # T-matica
    def kresli(self, T):
        body2 = []
        # otacanie
        a = np.pi / 6
        for b in t.body:
            bod2 = T.dot(b.as_np())  # bod ako (x,y,w)
            nx = bod2[0] / bod2[2]  # bod nx = x*w
            ny = bod2[1] / bod2[2]  # bod ny = y/w

            # cez maticu
            # stare
            # nx = T[0][0] * b.x + T[0][1] * b.y
            # ny = T[1][0] * b.x + T[1][1] * b.y

            # cez vzorceky na pevno
            # nx = b.x * np.cos(a) - b.y * np.sin(a)
            # ny = b.y * np.cos(a) + b.x * np.sin(a)

            body2.append(Bod(nx, ny))

        # kresli
        predosly = None
        for bod in body2:
            if predosly is not None:
                self.usecka(predosly.x, predosly.y, bod.x, bod.y)
            predosly = bod
        self.usecka(predosly.x, predosly.y, body2[0].x, body2[0].y)


sdl2.ext.init()

window = sdl2.ext.Window("Priklad Event Loop", size=(800, 600))
window.show()

plocha = window.get_surface()
BIELA = sdl2.ext.Color(255, 255, 255)
sdl2.ext.fill(plocha, BIELA)
window.refresh()

pixle = sdl2.ext.PixelView(plocha)

t = Tvar([Bod(50, 50), Bod(100, 50), Bod(100, 100), Bod(50, 100)], sdl2.ext.Color(255, 0, 0))

running = True
animuj = 0  # meniaci sa uhol
while running:
    events = sdl2.ext.get_events()
    for event in events:
        if event.type == sdl2.SDL_QUIT:
            running = False
            break

    # a=uhol
    a = np.pi / 6
    # otoc len
    T1 = np.array(
        [[np.cos(a), -np.sin(a), 0],
         [np.sin(a), np.cos(a), 0],
         [0, 0, 1]])

    # posun len
    T2 = np.array(
        [[1, 0, 400],
         [0, 1, 200],
         [0, 0, 1]])

    # spojenie T1 a T2
    T3 = T1.dot(T2)

    # posun v priestore
    K = np.array(
        [[1, 0, 0],
         [0, 1, 0],
         [0, 0, 1]])

    # otacanie okolo svojho stredu
    # stred 75 podla zadania

    animuj += 0.1
    # posun len
    O1 = np.array(
        [[1, 0, -75],
         [0, 1, -75],
         [0, 0, 1]])
    O2 = np.array(
        [[np.cos(animuj), -np.sin(animuj), 0],
         [np.sin(animuj), np.cos(animuj), 0],
         [0, 0, 1]])
    O3 = np.array(
        [[1, 0, 75],
         [0, 1, 75],
         [0, 0, 1]])
    toc = O3.dot(O2).dot(O1)
    sdl2.ext.fill(plocha, BIELA)
    t.kresli(toc)
    window.refresh()
    sdl2.SDL_Delay(10)
sdl2.ext.quit()