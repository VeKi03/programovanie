let words = [
    'STROM',
    'RUKAVICA',
    'HADICA',
];

let gameOverModal = new bootstrap.Modal(document.getElementById('gameover-modal'), {});

let activeWord = null;
let hangmanPhase = 0;

function start() {
    activeWord = words[Math.floor((Math.random() * words.length))];
    let activeWordArray = activeWord.split('');
    // nastavim fazu obesenca na 0
    hangmanPhase = 0;
    nakresliObesenca();

    // vymazat elementy pre pismenka
    document.getElementById('hangman-text').innerHTML = '';
    activeWordArray.forEach((e) => {
        let btn = document.createElement('div');
        btn.setAttribute('class', 'alert alert-info me-1 text-center');
        btn.setAttribute('data-letter', e);
        btn.innerHTML = '&nbsp;';
        document.getElementById('hangman-text').appendChild(btn);
    });

    // abeceda
    document.getElementById('hangman-keys').innerHTML = '';
    let abeceda = 'ABCČDĎEÉFGHIÍJKLĹĽMNŇOÓÔPQWSŠRŔTUÚVWXZŽ';

    abeceda.split('').forEach(e => {
        let btn = document.createElement('button');
        btn.setAttribute('class', 'btn btn-info me-1 mb-1 text-center');
        btn.setAttribute('data-letter', e);
        btn.innerHTML = e;

        btn.addEventListener('click', klikNaKlaves);

        document.getElementById('hangman-keys').appendChild(btn);
    })
}

function nakresliObesenca() {
    document.getElementById('hangman-img').src = `img/hang-${hangmanPhase}.png`;
}

function klikNaKlaves(event) {
    let pismeno = event.target.dataset.letter;
    let miesta = document.querySelectorAll(`div[data-letter=${pismeno}]`);
    console.log(miesta);
    if (miesta.length > 0) {
        miesta.forEach(e => {
            e.classList.add('alert-success');
            e.classList.remove('alert-info');
            e.innerHTML = e.dataset.letter;
            // klavesa
            event.target.classList.remove('btn-info');
            event.target.classList.add('btn-success');
        });
        // odoberiem uhadnute poismena z vybraneho slova
        activeWord = activeWord.replace(pismeno, '');

    } else {
        event.target.classList.add('btn-danger');
        event.target.classList.remove('btn-info');
        hangmanPhase++;
        nakresliObesenca();
    }
    console.log(activeWord);

    if (activeWord.length === 0) {
        let messageElm = document.getElementById('gameover-message');
        messageElm.innerHTML = 'Vyhral si!!!';
        messageElm.setAttribute('class', 'alert alert-success');
        gameOverModal.show();
    }

    if (hangmanPhase === 10) {
        let messageElm = document.getElementById('gameover-message');
        messageElm.innerHTML = 'Prehral si!!!';
        messageElm.setAttribute('class', 'alert alert-danger');
        gameOverModal.show();
    }
}

window.addEventListener('load', () => {
    document.getElementById('btn-start').addEventListener('click', () => {
        start();
    });

    document.getElementById('btn-restart').addEventListener('click', () => {
        start();
        gameOverModal.hide();
    });
    start();
})