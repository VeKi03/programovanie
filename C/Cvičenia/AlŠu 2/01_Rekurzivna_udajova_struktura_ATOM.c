#include <stdio.h>
#include <stdlib.h>
#define ML (ATOM*)malloc(sizeof(ATOM)) //Makro pre alok�ciu pam�te pre jeden prvok typu ATOM

//1. Vytvorte s�bor vstup.txt. S�bor bude obsahova� inform�cie knih�ch v kni�nici: inici�lky mena autora, priezvisko autora, n�zov knihy, rok vydania, po�et str�n.
//3. Vytvorte rekurz�vnu �dajov� �trukt�ru ATOM, ktor� bude obsahova� inform�ciu typu KNIHA a smern�k na typ ATOM.
//2. Vytvorte �dajov� typ KNIHA, ktor� bude sl��i� na ukladanie inform�cie o jednej knihe.
//4. Vytvorte typ smern�k na typ ATOM
//5. Pomocou �trukt�ry ATOM na��tajte prv� riadok �dajmi zo s�boru vstup.txt do pam�ti po��ta�a.
//6. Pokra�ujte podobne ako v �lohe 5.  V na��tavan� �al��ch riadkov zo s�boru do premenn�ch typu ATOM. Pou��vajte pritom pomocn� smern�k, ktor�m v�dy n�jdete naposledy vlo�en� at�m, za ktor� vlo��te nov� at�m.
//7. Pomocou pomocn�ho smern�ka vyp�te n�zvy v�etk�ch ulo�en�ch kn�h v at�moch na obrazovku po��ta�a.

// Defin�cia �trukt�ry pre inform�cie o knihe
typedef struct{
    char meno[8];
    char priezvisko[20];
    char nazov[50];
    int rok;
    int strany;
}KNIHA;

// Defin�cia rekurz�vnej �trukt�ry ATOM pre vytvorenie zoznamu kn�h
typedef struct atom{
    KNIHA k;
    struct atom *nasl; // Smern�k na �al�� prvok typu ATOM
}ATOM;


int main()
{
    // Na��tanie prv�ho riadku zo s�boru

    KNIHA p_k;
    FILE *f;
    ATOM *kniznica;

    // Otvorenie s�boru "vstup.txt" v re�ime na ��tanie
    f = fopen("vstup.txt","r");

    // Na��tanie inform�ci� o prvej knihe do premennej typu KNIHA
    fscanf(f,"%[^;];%[^;];%[^;];%d;%d\n",p_k.meno,p_k.priezvisko,p_k.nazov,&p_k.rok,&p_k.strany);
    printf("%s %s %s %d %d\n",p_k.meno,p_k.priezvisko,p_k.nazov,p_k.rok,p_k.strany);

    // Vytvorenie prv�ho at�mu v pam�ti a ulo�enie inform�ci� o knihe
    kniznica=ML; //ML = (ATOM*)malloc(sizeof(ATOM))
    kniznica->k=p_k;
    kniznica->nasl=NULL;
    printf("%s %s %s %d %d\n",kniznica->k.meno,kniznica->k.priezvisko,kniznica->k.nazov,kniznica->k.rok,kniznica->k.strany);


    // Na��tanie �al��ch riadkov zo s�boru a vytvorenie zoznamu
    ATOM *p=kniznica;
    //test konca s�boru pomocou makra feof()
    while(!feof(f)){
        // Alok�cia pam�te pre nov� at�m a nastavenie smern�ka na �al�� at�m
        p->nasl=ML;
        p=p->nasl;

        // Na��tanie inform�ci� o �al�ej knihe do nov�ho at�mu
        fscanf(f,"%[^;];%[^;];%[^;];%d;%d\n",p->k.meno,p->k.priezvisko,p->k.nazov,&p->k.rok,&p->k.strany);
        p->nasl=NULL;
    }
    fclose(f);

    // V�pis n�zvov v�etk�ch ulo�en�ch kn�h
    p=kniznica;
    while(p!=NULL){
        printf("%s %s %s %d %d\n",p->k.meno,p->k.priezvisko,p->k.nazov,p->k.rok,p->k.strany);
        p=p->nasl;
    }
    return 0;
}
