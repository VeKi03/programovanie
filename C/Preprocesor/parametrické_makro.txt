/*
 * Predmet: Programovanie 1
 * Program: Obsah kruhu - makra (ver. 1)
 * Autor: Miroslav Melichercik
 * Datum: 1.3.2022
 *
 * Program nacita polomer kruhu a vypocita jeho obsah s pouzitim makier
 */

#include <stdio.h>

#define PI 3.14
#define na_druhu(x) ((x) * (x))
//#define na_druhu(x) (x * x) // nie je spravne

int main()
{
  //definicia premennych
  float r, S, S2;
  //nacitanie vstupu - polomer
  printf("Zadaj polomer kruhu: ");
  scanf("%f", &r);
  //vypocet obsahu
  S = PI * na_druhu(r);
  S2 = PI * na_druhu(r+r);
  //vypis vysledku
  printf("Obsah kruhu je: %f\n", S);
  printf("Obsah kruhu 2 je: %f\n", S2);
  return(0);
}
