/* JAZDCOVA PRECHADZKA */

x-2,y-1     -----     x-2,y+1
              |
x-1,y-2   ---------   x-1,y+2
              |
Kon           *       Kon
              |
x+1,y-2   ---------   x+1,y+2
              |
x+2,y-1     -----     x+2,y+1

int sach[8][8];
int pocet;

void hladaj(int x, int y){
    pocet++;
    sach[x][y] = pocet;
    if(pocet == 64) return;

    if(sach[x+2][y+1] == 0 && x+2<8 && y+1<8) hladaj(x+2,y+1);
    if(sach[x+1][y+2] == 0 && x+1<8 && y+2<8) hladaj(x+1,y+2);
    if(sach[x-1][y+2] == 0 && x-1>0 && y+2<8) hladaj(x-1,y+2);
    if(sach[x-2][y+1] == 0 && x-2>0 && y+1<8) hladaj(x-2,y+1);
    if(sach[x-2][y-1] == 0 && x-2>0 && y-1>0) hladaj(x-2,y-1);
    if(sach[x-1][y-2] == 0 && x-1>0 && y-2>0) hladaj(x-1,y-2);
    if(sach[x+1][y-2] == 0 && x+1<8 && y-2>0) hladaj(x+1,y-2);
    if(sach[x+2][y-1] == 0 && x+2<8 && y-1>8) hladaj(x+2,y-1);

    sach[x][y]=0;
    pocet--;
}

/* ROBOT V BLUDISKU */
          x-1,y

          -||-
         |    |
x,y-1   -      -    x,y+1
        -      -
         |    |
          -||-

          x+1,y

//vychod = MAX-1,MAX-1

#define MAX 8
int bludisko[MAX][MAX];
int nasiel;

void hladaj(int x, int y){
    bludisko[x][y]='.';

    if(nasiel || x==MAX-1 && y==MAX-1){
        nasiel = 1;
        return;
    }

    if(!nasiel && x<MAX && bludisko[x+1][y]==' ') hladaj(x+1,y);
    if(!nasiel && y<MAX && bludisko[x][y+1]==' ') hladaj(x,y+1);
    if(!nasiel && x>0 && bludisko[x-1][y]==' ') hladaj(x-1,y);
    if(!nasiel && y>0 && bludisko[x][y-1]==' ') hladaj(x,y-1);

    if(nasiel)return;
    bludisko[x][y]=' ';
}

/* PROBLEM 8 DAM */

int sach[8][8];
int pocet;
int st[8];
int d1[15],d2[15];

void hladaj(int r){
    if(r==8) pocet++;
    int i;
    for(i=0;i<8;i++){
        if(!st[i] && !d1[r+i] && !d2[r-i+7]){
            st[i]=1;
            d1[r+i]=1;
            d2[r-i+7]=1;
            sach[r][i]='*';
            hladaj(r+1);
            st[i]=0;
            d1[r+i]=0;
            d2[r-i+7]=0;
            sach[r][i]='-';
        }
    }
}

/* LINEARNE VYHLADAVANIE */

typedef struct{
    char m[10];
    char p[20];
    int v;
}CLOVEK;

int hladaj(CLOVEK *c, int _c, char *x){
    int i;
    while(i<_c){
        if(strcmp(c[i].p,x)==0)
            return i;
        i++;
    }
    return -1;
}

// pole
int hladaj(char p[][20], int _c, char x[]){
    int i;
    while(i<_c){
        if(strcmp(p[i],x)==0)
            return i;
        i++;
    }
    return -1;
}

/* BINARNE VYHLADAVANIE NEREKURZIVNE */

int hladaj(CLOVEK *c, int _c, char *x){
    int lava=0, prava=_c-1, stred;
    while(lava<=prava){
        stred = floor((lava+prava)/2);
        if(strcmp(c[stred].p,x)==0) return stred;
        if(strcmp(c[stred].p,x)>0)
            prava = stred-1;
        else
            lava = stred+1;
    }
    return -1;
}

/* BINARNE VYHLADAVANIE REKURZIVNE */

int hladaj(CLOVEK *c, int lava, int prava, char *x){
    if(lava>prava)return -1;
    else{
        int stred = (lava+prava)/2;
        if(strcmp(c[stred].p,x)==0)return stred;
        else
            if(strcmp(c[stred].p,x)>0)
                return hladaj(c,lava,stred-1,x);
            else
                return hladaj(c,stred+1,prava,x);
    }
}

/* BINARNY VYHLADAVACI STROM NEREKURZIVNE */

#define ML (UZOL*)malloc(sizeof(UZOL))

typedef struct uzol{
    char slovo[20];
    struct uzol *L,*P;
}UZOL;

int hladaj(UZOL *u, char *s){
    UZOL *p = *u;
    while(1){
        if(strcmp(p->slovo,s)==0)
            return 1;
        else
            if(strcmp(p->slovo,s)>0)
                if(p->L==NULL)
                    return -1;
                else
                    p=p->L;
            else
                if(p->P==NULL)
                    return -1;
                else
                    p=p->P;
    }
}

/* BINARNY VYHLADAVACI STROM REKURZIVNE */

#define ML (UZOL*)malloc(sizeof(UZOL))

typedef struct uzol{
    char slovo[20];
    struct uzol *L,*P;
}UZOL;

int hladaj(UZOL *u, char *s){
    if(strcmp(u->slovo,s)==0)return 1;
    if(strcmp(u->slovo,s)>0){
        if(u->L==NULL)
            return -1;
        return hladaj(u->L,s);
    }
    else{
        if(u->P==NULL)
            return -1;
        return hladaj(u->P,s);
    }
}




